import React, { useState, useEffect } from 'react';

const ServiceHistory = ({ service_history }) => {
    const [appointments, setAppointments] = useState([]);
    const [vin, setVin] = useState('');

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin({ vin: value });
    }
    // async handleSubmit(event) {
    //     event.preventDefault
    // }

    // const [service_appointments, setAppointments] = useState([]);

    //     useEffect(() => {
    //         fetch('http://localhost:8080/api/appointments/')
    //             .then(response => response.json())
    //             .then(data => setAppointments(data.service_appointments))
    //             .catch(e => console.error('error: ', e))
    //     }, [])

    // useEffect(() => {
    //     fetch('http://localhost:8080/api/appointments/')
    //         .then(response => response.json())
    //         .then(data => setFinished(data.service_appointments.is_finished))
    //         .catch(e => console.error('error: ', e))
    // }, [])
    // const getAppointments = async (event) => {
    //     const value = event.target.value;
    //     const url = ('http://localhost:8080/api/service-history/?' +
    //         new URLSearchParams({ vin: value }).toString()
    //     );
    //     const result = await fetch(url)
    //         .then(response => response.json())
    // }
    // getAppointments();


    return (
        <>
            <h1>Service History</h1>
            <input onChange={handleVinChange} required id="vin" name="vin" className="form-control" value="vin" type="search" placeholder="VIN" />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason</th>
                        <th>Technician</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.technician.technician_name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ServiceHistory;