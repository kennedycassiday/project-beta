import React, { useState, useEffect } from 'react';

const ServiceAppointmentForm = () => {
    const [vin, setVin] = useState('');
    const [customer_name, setCustomerName] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState([]);
    const [technician, setTechnicianNumber] = useState('');
    const [tech, setTechnician] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/api/technicians/')
            .then(response => response.json())
            .then(data => {
                setTechnician(data.technicians)
            })
            .catch(e => console.error('error: ', e))
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault();
        const newServiceAppointment = {
            'vin': vin,
            'customer_name': customer_name,
            'date': date,
            'time': time,
            'reason': reason,
            'technician': technician,
        }

        const serviceAppointmentsURL = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newServiceAppointment),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(serviceAppointmentsURL, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setVin('');
                setCustomerName('');
                setDate('');
                setTime('');
                setReason('');
                setTechnicianNumber('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }


    const handleCustomerNameChange = (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleTechnicianNumberChange = (event) => {
        const value = event.target.value;
        setTechnicianNumber(value);
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-model-form">
                        <div className="form-floating mb-3">
                            <input value={vin} onChange={handleVinChange} required type="text" name="vin" id="vin" className="form-control" />
                            <label>VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={customer_name} onChange={handleCustomerNameChange} required type="text" name="customer_name" id="customer_name" className="form-control" />
                            <label>Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={date} onChange={handleDateChange} required type="date" name="date" id="date" className="form-control" />
                            <label>Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={time} onChange={handleTimeChange} required type="time" name="time" id="time" className="form-control" />
                            <label>Time</label>
                        </div>


                        <div className="form-floating mb-3">
                            <select onChange={handleTechnicianNumberChange} value={technician} required id="technician" name="technician" className="form-select">
                                <option value="">Choose a technician</option>
                                {tech.map(technician => {
                                    return (
                                        <option key={technician.employee_number} value={technician.employee_number}> {technician.technician_name} </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={reason} onChange={handleReasonChange} required type="text" name="reason" id="reason" className="form-control" />
                            <label>Reason</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ServiceAppointmentForm;
