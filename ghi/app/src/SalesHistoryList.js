import React, { useState, useEffect } from 'react';

function SalesHistoryList ( {sale_records} ) {
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('')

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
      }

    const getSalespeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setSalespeople(data.salespeople);
        }
      }


      useEffect(() => {
        getSalespeople();
      }, []);

    return (
          <div className="shadow p-4 mt-4">
            <h1>Sales person history</h1>
            <div className="mb-3">
                <select value={salesperson} onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select">
                  <option value="">Choose a sales person</option>
                  {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.name}>{salesperson.name}</option>
                    );
                  })}
                </select>
            </div>
                <table className=" table table-striped">
                    <thead>
                        <tr>
                        <th>Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sale_records.filter(sale => sale.salesperson.name === salesperson).map((sale_record, idx) => {
                          return (
                            <tr key={idx}>
                            <td>{ sale_record.salesperson.name }</td>
                            <td>{ sale_record.customer.name }</td>
                            <td>{ sale_record.automobile.vin }</td>
                            <td>{ sale_record.sale_price }</td>
                            </tr>
                          )}) }
                    </tbody>
                </table>
          </div>
      );

}

export default SalesHistoryList;
