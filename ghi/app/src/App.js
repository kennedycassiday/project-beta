import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonForm from './SalespersonForm';
import CustomerForm from './CustomerForm';
import SalesrecordForm from './SalesrecordForm';
import ManufacturerList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ModelsList';
import { useState, useEffect } from "react";
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import VehicleForm from './VehicleForm';
import SalesHistoryList from './SalesHistoryList';
import SalesList from './SalesList';
import ServiceHistory from './ServiceHistory';
import ServiceAppointmentList from './ServiceAppointmentList';
import TechnicianForm from './TechnicianForm';
import ServiceAppointmentForm from './ServiceAppointmentForm';

function App(props) {
  const [manufacturers, setManufacturers] = useState([])
  const [models, setModels] = useState([])
  const [sale_records, setSale_records] = useState([])
  const [service_history, setService_History] = useState([])

  const getModels = async () => {
    const url = 'http://localhost:8100/api/models/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const models = data.models
      setModels(models)
    }
  }

  const getManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const manufacturers = data.manufacturers
      setManufacturers(manufacturers)
    }
  }

  const getSale_records = async () => {
    const url = 'http://localhost:8090/api/salesrecords/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const sale_records = data.sale_records
      setSale_records(sale_records)
    }
  }

  // const getService_History = async () => {
  //   const url = 'http://localhost:8080/api/service-history/'
  //   const response = await fetch(url);
  //   if (response.ok) {
  //     const data = await response.json();
  //     const service_history = data.service_history
  //     setService_History(service_history)
  //   }
  // }


  useEffect(() => {
    getManufacturers();
    getModels();
    getSale_records();
    // getService_History();
  }, [])



  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="automobiles" element={<AutomobileList />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />
          <Route path="vehicles/new" element={<VehicleForm getModels={getModels} />} />
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="services" element={<ServiceAppointmentList />} />
          <Route path="service/new" element={<ServiceAppointmentForm />} />
          {/* <Route path="service/history" element={<ServiceHistory getService_History={getService_History} />} /> */}
          <Route path="/salesperson/new" element={<SalespersonForm />}></Route>
          <Route path="/customer/new" element={<CustomerForm />}></Route>
          <Route path="/salesrecords/new" element={<SalesrecordForm getSale_records={getSale_records} />}></Route>
          <Route path="/salesrecords" element={<SalesList sale_records={sale_records} getSale_records={getSale_records} />}></Route>
          <Route path="/saleshistory" element={<SalesHistoryList sale_records={sale_records} />}></Route>
          <Route path="/manufacturer" element={<ManufacturerList manufacturers={manufacturers} getManufacturers={getManufacturers} />}></Route>
          <Route path="/manufacturer/new" element={<ManufacturerForm getManufacturers={getManufacturers} />}></Route>
          <Route path="/model" element={<ModelsList models={models} getModels={getModels} />}></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
