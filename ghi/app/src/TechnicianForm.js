import React, { useState, useEffect } from 'react';

const TechnicianForm = () => {
    const [technicianName, setTechnicianName] = useState('');
    const [technicianEmployeeNumber, setTechnicianEmployeeNumber] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        const newTechnician = {
            'technician_name': technicianName,
            'employee_number': technicianEmployeeNumber,
        }

        const techniciansURL = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newTechnician),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(techniciansURL, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setTechnicianName('');
                setTechnicianEmployeeNumber('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleTechnicianNameChange = (event) => {
        const value = event.target.value;
        setTechnicianName(value);
    }


    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setTechnicianEmployeeNumber(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create new technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input value={technicianName} onChange={handleTechnicianNameChange} required type="text" name="name" id="name" className="form-control" />
                            <label>Technician Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={technicianEmployeeNumber} onChange={handleEmployeeNumberChange} required type="text" name="employee_number" id="employee_number" className="form-control" />
                            <label>Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;